#pragma once
#include <string>
#include <nlohmann/json_fwd.hpp>

namespace mh {
namespace tools {

namespace arc {
class Reader;
}

extern auto json(const arc::Reader& obj) -> nlohmann::ordered_json;
extern auto dump(const arc::Reader& obj, const int indent = -1) -> std::string;

}; /// namespace tools
}; /// namespace mh
