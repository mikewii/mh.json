#include "Tools/arc.hpp"
#include "helpers.hpp"
#include <libmh/tools/arc/reader.hpp>
#include <libmh/MH/Resource.hpp>
#include <nlohmann/json.hpp>

namespace mh {
namespace tools {

auto getExtention(const u32 hash) -> std::string
{
    const char* str = mh::sResource::getExtention(hash);

    std::string out(str ? str : "");

    return out;
}

auto parseHeaderHEX(const arc::Header& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    j["Magic"] = asHex(obj.Magic);
    j["Version"] = asHex(obj.Version);
    j["FilesNum"] = asHex(obj.FilesNum);

    return j;
}

auto parseARCFileHeaderHEX(const arc::FileHeader& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    j["FilePath"] = obj.FilePath;
    j["ResourceHash"] = asHex(obj.ResourceHash);
    j["Extention"] = getExtention(obj.ResourceHash);
    j["CompressedSize"] = asHex(obj.CompressedSize);
    j["DecompressedSize"] = asHex(obj.UncompressedSize);
    j["Flags"] = asHex(obj.Flags);
    j["pZData"] = asHex(obj.pZData);

    return j;
}

auto parseARCFileHeadersHEX(const std::vector<arc::FileHeader>& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    for (auto i = 0u; i < obj.size(); i++) {
        const arc::FileHeader& header = obj[i];
        std::stringstream ss;

        ss << "0x" << std::hex << i;

        j[ss.str()] = parseARCFileHeaderHEX(header);
    }

    return j;
}

auto json(const arc::Reader& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    j["Header"] = parseHeaderHEX(obj.getHeader());
    j["ARCFileHeaders"] = parseARCFileHeadersHEX(obj.getFileHeaders());

    return j;
}

auto dump(const arc::Reader& obj, const int indent) -> std::string
{
    return json(obj).dump(indent);
}

}; /// namespace tools
}; /// namespace mh
