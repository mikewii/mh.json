#include "Tools/parse.hpp"

#include <nlohmann/json.hpp>

#include <libmh/MHXX/Extentions/esl.hpp>
#include <libmh/MHXX/Extentions/qtds.hpp>
#include <libmh/MHXX/Extentions/gmd.hpp>
#include <libmh/MHXX/Extentions/qdl.hpp>
#include <libmh/MHXX/Extentions/qdp.hpp>
#include <libmh/MHXX/Extentions/rem.hpp>
#include <libmh/MHXX/Extentions/sem.hpp>
#include <libmh/MHXX/Extentions/sup.hpp>
#include <libmh/MH3U/Extentions/gmd.hpp>
#include <libmh/MH4U/Extentions/lmd.hpp>

#include "MHXX/Extentions/esl.hpp"
#include "MHXX/Extentions/qtds.hpp"
#include "MHXX/Extentions/gmd.hpp"
#include "MHXX/Extentions/qdl.hpp"
#include "MHXX/Extentions/qdp.hpp"
#include "MHXX/Extentions/rem.hpp"
#include "MHXX/Extentions/sem.hpp"
#include "MHXX/Extentions/sup.hpp"
#include "MH3U/Extentions/gmd.hpp"
#include "MH4U/Extention/lmd.hpp"

#include <fstream>

template < typename T
         , bool(*parseFun)(std::istream&, T&) noexcept
         , nlohmann::ordered_json(*jsonFun)(const T&)>
nlohmann::ordered_json commonParse(const std::string& path)
{
    std::ifstream in(path, std::ios::in | std::ios::binary);
    nlohmann::ordered_json out;
    T obj;

    parseFun(in, obj);

    out = jsonFun(obj);

    // hack for mh3u gmd
    if constexpr (!std::is_same<T, mh::mhxx::gmd::cGMD>::value) {
        if (out.empty()) {
            mh::mh3u::gmd::cGMD obj;

            mh::mh3u::gmd::parse(in, obj);

            return mh::mh3u::gmd::json(obj);
        }
    }

    return out;
}

std::map<std::pair<std::string, u32>, SupportedExtentions::function> SupportedExtentions::map = {
    {  { "esl", mh::mhxx::esl::RESOURCE_HASH }
     , &commonParse<mh::mhxx::esl::cESL,mh::mhxx::esl::parse, mh::mhxx::esl::json> },
    {  { "qtds", mh::mhxx::qtds::RESOURCE_HASH }
     , &commonParse<mh::mhxx::qtds::cQTDS, mh::mhxx::qtds::parse, mh::mhxx::qtds::json> },
    {  { "gmd", mh::mhxx::gmd::RESOURCE_HASH }
     , &commonParse<mh::mhxx::gmd::cGMD, mh::mhxx::gmd::parse, mh::mhxx::gmd::json> },
    {  { "qdl", mh::mhxx::qdl::RESOURCE_HASH }
     , &commonParse<mh::mhxx::qdl::cQDL, mh::mhxx::qdl::parse, mh::mhxx::qdl::json> },
    {  { "qdp", mh::mhxx::qdp::RESOURCE_HASH }
     , &commonParse<mh::mhxx::qdp::cQDP, mh::mhxx::qdp::parse, mh::mhxx::qdp::json> },
    {  { "rem", mh::mhxx::rem::RESOURCE_HASH }
     , &commonParse<mh::mhxx::rem::cREM, mh::mhxx::rem::parse, mh::mhxx::rem::json> },
    {  { "sem", mh::mhxx::sem::RESOURCE_HASH }
     , &commonParse<mh::mhxx::sem::cSEM, mh::mhxx::sem::parse, mh::mhxx::sem::json> },
    {  { "sup", mh::mhxx::sup::RESOURCE_HASH }
     , &commonParse<mh::mhxx::sup::cSUP, mh::mhxx::sup::parse, mh::mhxx::sup::json> },
    {  { "lmd", mh::mh4u::lmd::RESOURCE_HASH }
     , &commonParse<mh::mh4u::lmd::cLMD, mh::mh4u::lmd::parse, mh::mh4u::lmd::json> }
};

auto SupportedExtentions::contains(const std::string &val) const noexcept -> bool
{
    for (auto it = map.begin(); it != map.end(); it++)
        if (val == it->first.first)
            return true;

    return false;
}

auto SupportedExtentions::contains(const u32 val) const noexcept -> bool
{
    for (auto it = map.begin(); it != map.end(); it++)
        if (val == it->first.second)
            return true;

    return false;
}

auto SupportedExtentions::operator [] (const u32 val) const noexcept(false) -> SupportedExtentions::function&
{
    for (auto it = map.begin(); it != map.end(); it++)
        if (val == it->first.second)
            return it->second;

    throw std::out_of_range("map out of range");
}

auto SupportedExtentions::operator [] (const std::string &val) const noexcept(false) -> SupportedExtentions::function&
{
    for (auto it = map.begin(); it != map.end(); it++)
        if (val == it->first.first)
            return it->second;

    throw std::out_of_range("map out of range");
}
