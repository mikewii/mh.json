#include "dragndrop.hpp"
#include <QEvent>
#include <QDragEnterEvent>
#include <QDragLeaveEvent>
#include <QMimeData>

bool DragNDrop::eventFilter(QObject *watched, QEvent *event)
{
    if (event->type() == QEvent::DragEnter) {
        handleDragEnter(dynamic_cast<QDragEnterEvent *>(event));
    } else if (event->type() == QEvent::Drop) {
        handleDrop(dynamic_cast<QDropEvent *>(event));
    }

    return QObject::eventFilter(watched, event);
}

void DragNDrop::handleDragEnter(QDragEnterEvent *event)
{
    if (event->mimeData()->hasUrls())
        event->acceptProposedAction();
}

void DragNDrop::handleDrop(QDropEvent *event)
{
    if (event->mimeData()->hasUrls()) {
        event->acceptProposedAction();

        emit fileDrop(event->mimeData()->urls()[0]);
    }
}
