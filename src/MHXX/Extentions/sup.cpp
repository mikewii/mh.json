#include "sup.hpp"
#include "common.hpp"

#include <libmh/MHXX/Extentions/sup.hpp>
#include <libmh/MHXX/Items/Items.hpp>
#include <nlohmann/json.hpp>

namespace mh {
namespace mhxx {
namespace sup {

auto json(const cSUP& obj) -> nlohmann::ordered_json
{
    if (!obj.isValid())
        return {};

    nlohmann::ordered_json j;
    nlohmann::ordered_json::array_t jItems;

    const auto& data = obj.getSupply();

    j["Magic"] = data.Magic;
    j["Version"] = data.Version;

    for (const auto& item : data.Items) {
        nlohmann::json jItem;

        jItem["ID"] = parseItemIDHEX(item.ID);
        jItem["Ammount"] = item.Ammount;
        jItem["Unk"] = item.Unk;

        jItems.emplace_back(std::move(jItem));
    }

    j["Items"] = jItems;

    return j;
}

auto dump(const cSUP &obj, const int indent) -> std::string
{
    return json(obj).dump(indent);
}

}; /// namespace sup
}; /// namespace mhxx
}; /// namespace mh
