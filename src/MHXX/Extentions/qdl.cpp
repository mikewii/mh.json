#include "qdl.hpp"
#include <libmh/MHXX/Extentions/qdl.hpp>
#include <nlohmann/json.hpp>

namespace mh {
namespace mhxx {
namespace qdl {

auto json(const cQDL& obj) -> nlohmann::ordered_json
{
    if (!obj.isValid())
        return {};

    nlohmann::ordered_json j;
    nlohmann::ordered_json::array_t jItems;

    const auto& data = obj.getQuestDataLink();

    j["Magic"] = data.Magic;
    j["Version"] = data.Version;

    for (const auto& item : data.Items) {
        nlohmann::ordered_json jItem;

        jItem["Name"] = item.Name;
        jItem["Resource"] = item.Resource;

        jItems.emplace_back(std::move(jItem));
    }

    j["Items"] = jItems;

    return j;
}

auto dump(const cQDL &obj, const int indent) -> std::string
{
    return json(obj).dump(indent);
}

}; /// namespace qdl
}; /// namespace mhxx
}; /// namespace mh
