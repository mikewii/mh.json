#include "esl.hpp"
#include "helpers.hpp"
#include <libmh/MHXX/Extentions/esl.hpp>
#include <nlohmann/json.hpp>
#include <sstream>
#include <type_traits>

namespace mh {
namespace mhxx {
namespace esl {

inline auto parseHeaderDEC(nlohmann::ordered_json& j, const Header& header) -> void
{
    nlohmann::ordered_json::array_t arr;

    j["magic"] = header.Magic;
    j["version"] = header.Version;
    j["Padding0"] = header.Padding0;

    arr.insert(  arr.end()
               , std::begin(header.pESD)
               , std::end(header.pESD));

    j["pESD"] = arr;
}

inline auto parseHeaderHEX(nlohmann::ordered_json& j, const Header& header) -> void
{
    nlohmann::ordered_json::array_t arr;

    j["magic"] = asHex(header.Magic);
    j["version"] = asHex(header.Version);
    j["Padding0"] = asHex(header.Padding0);

    for (auto it = std::begin(header.pESD); it != std::end(header.pESD); it++)
        arr.emplace_back(asHex(*it));

    j["pESD"] = arr;
}

inline auto parseESDVectorDEC(nlohmann::ordered_json& j, const std::vector<cESL::ESD>& esd) -> void
{
    for (auto i = 0u; i < esd.size(); i++) {
        nlohmann::ordered_json jWrapper;
        nlohmann::ordered_json jESD;
        nlohmann::ordered_json jEnemySmallVector;

        jESD["Magic0"] = esd[i].first.Magic0;
        jESD["Magic1"] = esd[i].first.Magic1;
        jESD["EmSmallNum"] = esd[i].first.EnemySmallNum;

        const auto& emSmallDataVector = esd[i].second;

        for (auto j = 0u; j < emSmallDataVector.size(); j++) {
            nlohmann::ordered_json jEmSmallData;
            nlohmann::ordered_json jID;
            nlohmann::ordered_json jPosition;

            const auto& emSmallData = emSmallDataVector[j];

            jID["ID"] = emSmallData.EnemyID.ID;
            jID["IDSub"] = emSmallData.EnemyID.IDSub;
            jID["IDStr"] = sEnemy::getStr(emSmallData.EnemyID);
            jEmSmallData["ID"] = jID;

            jEmSmallData["Unk0_0"] = emSmallData.Unk0[0];
            jEmSmallData["Unk0_1"] = emSmallData.Unk0[1];
            jEmSmallData["SpawnCondition"] = emSmallData.SpawnCondition;

            jEmSmallData["Area"] = emSmallData.Area;

            jEmSmallData["Unk1"] = emSmallData.Unk1;

            jPosition["X"] = emSmallData.Position.X;
            jPosition["Y"] = emSmallData.Position.Y;
            jPosition["Z"] = emSmallData.Position.Z;
            jPosition["R"] = emSmallData.Position.R;

            jEmSmallData["Position"] = jPosition;

            jEmSmallData["Unk2_0"] = emSmallData.Unk2[0];
            jEmSmallData["Unk2_1"] = emSmallData.Unk2[1];
            jEmSmallData["Unk2_2"] = emSmallData.Unk2[2];

            jEnemySmallVector[std::to_string(j)] = jEmSmallData;
        }

        jWrapper["ESData"] = jESD;
        jWrapper["EmSmallDataVector"] = jEnemySmallVector;

        j[std::to_string(i)] = jWrapper;
    }
}

inline auto parseESDVectorHEX(nlohmann::ordered_json& j, const std::vector<cESL::ESD>& esd) -> void
{
    for (auto i = 0u; i < esd.size(); i++) {
        nlohmann::ordered_json jWrapper;
        nlohmann::ordered_json jESD;
        nlohmann::ordered_json jEnemySmallVector;
        std::stringstream ss;

        jESD["Magic0"] = asHex(esd[i].first.Magic0);
        jESD["Magic1"] = asHex(esd[i].first.Magic1);
        jESD["EmSmallNum"] = asHex(esd[i].first.EnemySmallNum);

        const auto& emSmallDataVector = esd[i].second;

        for (auto j = 0u; j < emSmallDataVector.size(); j++) {
            nlohmann::ordered_json jEmSmallData;
            nlohmann::ordered_json jID;
            nlohmann::ordered_json jPosition;
            std::stringstream ss;

            const auto& emSmallData = emSmallDataVector[j];

            jID["ID"] = asHex(emSmallData.EnemyID.ID);
            jID["IDSub"] = asHex(emSmallData.EnemyID.IDSub);
            jID["IDStr"] = sEnemy::getStr(emSmallData.EnemyID);
            jEmSmallData["ID"] = jID;

            jEmSmallData["Unk0_0"] = asHex(emSmallData.Unk0[0]);
            jEmSmallData["Unk0_1"] = asHex(emSmallData.Unk0[1]);
            jEmSmallData["SpawnCondition"] = asHex(emSmallData.SpawnCondition);

            jEmSmallData["Area"] = asHex(emSmallData.Area);

            jEmSmallData["Unk1"] = asHex(emSmallData.Unk1);

            jPosition["X"] = asHex(emSmallData.Position.X);
            jPosition["Y"] = asHex(emSmallData.Position.Y);
            jPosition["Z"] = asHex(emSmallData.Position.Z);
            jPosition["R"] = asHex(emSmallData.Position.R);

            jEmSmallData["Position"] = jPosition;

            jEmSmallData["Unk2_0"] = asHex(emSmallData.Unk2[0]);
            jEmSmallData["Unk2_1"] = asHex(emSmallData.Unk2[1]);
            jEmSmallData["Unk2_2"] = asHex(emSmallData.Unk2[2]);

            ss << "0x" << std::hex << j;

            jEnemySmallVector[ss.str()] = jEmSmallData;
        }

        jWrapper["ESData"] = jESD;
        jWrapper["EmSmallDataVector"] = jEnemySmallVector;

        ss << "0x" << std::hex << i;

        j[ss.str()] = jWrapper;
    }
}

auto json(const cESL& obj) -> nlohmann::ordered_json
{
    if (!obj.isValid())
        return {};

    nlohmann::ordered_json j;
    nlohmann::ordered_json jHeader;
    nlohmann::ordered_json jESD;

    const auto& header = obj.getESLHeader();
    const auto& esdVec = obj.getESDVector();

    parseHeaderHEX(jHeader, header);
    parseESDVectorHEX(jESD, esdVec);

    j["header"] = jHeader;
    j["ESD"] = jESD;

    return j;
}

auto dump(const cESL &obj, const int indent) -> std::string
{
    return json(obj).dump(indent);
}

}; /// namespace esl
}; /// namespace mhxx
}; /// namespace mh
