#pragma once
#include <string>
#include <nlohmann/json_fwd.hpp>

namespace mh {
namespace mhxx {
namespace qtds {

class cQTDS;

extern auto json(const cQTDS& obj) -> nlohmann::ordered_json;
extern auto dump(const cQTDS& obj, const int indent = -1) -> std::string;

}; /// namespace qtds
}; /// namespace mhxx
}; /// namespace mh
