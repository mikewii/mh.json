#include "rem.hpp"
#include <libmh/MHXX/Extentions/rem.hpp>
#include <nlohmann/json.hpp>

namespace mh {
namespace mhxx {
namespace rem {

auto json(const cREM& obj) -> nlohmann::ordered_json
{
    if (!obj.isValid())
        return {};

    nlohmann::ordered_json j;
    nlohmann::ordered_json::array_t jFlags;
    nlohmann::ordered_json::array_t jFlagNums;
    nlohmann::ordered_json::array_t jItems;

    const auto& data = obj.getRewardEm();

    j["Magic"] = data.Magic;
    j["Version"] = data.Version;

    for (const auto& flag : data.Flags) {
        nlohmann::ordered_json jFlag;

        jFlag["Flag"] = flag.Flag;
        jFlag["ItemNum"] = flag.ItemNum;

        jFlags.emplace_back(std::move(jFlag));
    }

    for (auto i = 0u; i < data.FLAGNUMS_MAX; i++) {
        nlohmann::ordered_json jFlagNum;

        jFlagNum[std::to_string(i)] = data.FlagNums[i];

        jFlagNums.emplace_back(std::move(jFlagNum));
    }

    for (const auto& item : data.Items) {
        nlohmann::ordered_json jItem;

        jItem["ID"] = item.ID;
        jItem["Ammount"] = item.Ammount;
        jItem["Rate"] = item.Rate;

        jItems.emplace_back(std::move(jItem));
    }

    j["Flags"] = jFlags;
    j["FlagNums"] = jFlagNums;
    j["Items"] = jItems;

    return j;
}

auto dump(const cREM &obj, const int indent) -> std::string
{
    return json(obj).dump(indent);
}

}; /// namespace rem
}; /// namespace mhxx
}; /// namespace mh
