#pragma once
#include <string>
#include <nlohmann/json_fwd.hpp>

namespace mh {
namespace mh3u {
namespace gmd {

class cGMD;

extern auto json(const cGMD& obj) -> nlohmann::ordered_json;
extern auto dump(const cGMD& obj, const int indent = -1) -> std::string;

}; /// namespace gmd
}; /// namespace mh3u
}; /// namespace mh
