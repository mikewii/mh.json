#include "gmd.hpp"
#include "helpers.hpp"
#include <libmh/MH3U/Extentions/gmd.hpp>
#include <nlohmann/json.hpp>
#include <sstream>

namespace mh {
namespace mh3u {
namespace gmd {

inline auto parseHeaderDEC(nlohmann::ordered_json& j, const Header& header) -> void
{
    j["Magic"] = header.Magic;
    j["Version"] = header.Version;

    j["Unk0"] = header.Unk0;

    j["LabelsNum"] = header.LabelsNum;
    j["ItemsNum"] = header.ItemsNum;
    j["LabelsSize"] = header.LabelsSize;
    j["ItemsSize"] = header.ItemsSize;

    j["FilenameSize"] = header.FilenameSize;
}

inline auto parseHeaderHEX(nlohmann::ordered_json& j, const Header& header) -> void
{
    j["Magic"] = asHex(header.Magic);
    j["Version"] = asHex(header.Version);

    j["Unk0"] = asHex(header.Unk0);

    j["LabelsNum"] = asHex(header.LabelsNum);
    j["ItemsNum"] = asHex(header.ItemsNum);
    j["LabelsSize"] = asHex(header.LabelsSize);
    j["ItemsSize"] = asHex(header.ItemsSize);

    j["FilenameSize"] = asHex(header.FilenameSize);
}

inline auto parseAdvanced1DEC(nlohmann::ordered_json& j, const std::vector<sGMDAdvanced1>& adv1) -> void
{
    for (auto i = 0u; i < adv1.size(); i++) {
        const sGMDAdvanced1& item = adv1[i];
        nlohmann::ordered_json::array_t arr;

        arr.insert(  arr.end()
                   , std::begin(item.Unk)
                   , std::end(item.Unk));

        j[std::to_string(i)] = std::move(arr);
    }
}

inline auto parseAdvanced1HEX(nlohmann::ordered_json& j, const std::vector<sGMDAdvanced1>& adv1) -> void
{
    for (auto i = 0u; i < adv1.size(); i++) {
        const sGMDAdvanced1& item = adv1[i];
        nlohmann::ordered_json::array_t arr;
        std::stringstream ss;

        ss << "0x" << std::hex << i;

        for (auto it = std::begin(item.Unk); it != std::end(item.Unk); it++) {
            std::stringstream ss;

            ss << "0x" << std::hex << *it;

            arr.emplace_back(ss.str());
        }

        j[ss.str()] = std::move(arr);
    }
}

inline auto parseStringVectorDEC(nlohmann::ordered_json& j, const std::vector<std::string>& vec) -> void
{
    for (auto i = 0u; i < vec.size(); i++)
        j[std::to_string(i)] = vec[i];
}

inline auto parseStringVectorHEX(nlohmann::ordered_json& j, const std::vector<std::string>& vec) -> void
{
    for (auto i = 0u; i < vec.size(); i++) {
        std::stringstream ss;

        ss << "0x" << std::hex << i;

        j[ss.str()] = vec[i];
    }
}

auto json(const cGMD& obj) -> nlohmann::ordered_json
{
    if (!obj.isValid())
        return {};

    nlohmann::ordered_json j;
    nlohmann::ordered_json jHeader;
    nlohmann::ordered_json jAdvanced1;
    nlohmann::ordered_json::array_t jAdvanced2;
    nlohmann::ordered_json jLabels;
    nlohmann::ordered_json jItems;

    const auto& header = obj.getHeader();
    const auto& filename = obj.getFilename();
    const auto& items = obj.getItems();

    parseHeaderHEX(jHeader, header);
    parseStringVectorHEX(jItems, items);

    j["Header"] = jHeader;
    j["Filename"] = filename;

    if (!obj.getAdvanced1().empty()) {
        const auto& labels = obj.getLabels();
        const auto& advanced1 = obj.getAdvanced1();

        parseStringVectorHEX(jLabels, labels);
        parseAdvanced1HEX(jAdvanced1, advanced1);

        j["Advanced1"] = jAdvanced1;
        j["Labels"] = jLabels;
    }

    j["Items"] = jItems;

    return j;
}

auto dump(const cGMD &obj, const int indent) -> std::string
{
    return json(obj).dump(indent);
}

}; /// namespace gmd
}; /// namespace mh3u
}; /// namespace mh
