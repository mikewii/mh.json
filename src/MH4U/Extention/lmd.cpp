#include "lmd.hpp"
#include "helpers.hpp"
#include <libmh/MH4U/Extentions/lmd.hpp>
#include <nlohmann/json.hpp>
#include <sstream>
#include <locale>
#include <codecvt>

namespace mh {
namespace mh4u {
namespace lmd {

inline auto parseHeaderDEC(const Header& header) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    j["Magic"] = header.Magic;
    j["Version"] = header.Version;

    j["Data0Num"] = header.Data0Num;
    j["Data1Num"] = header.Data1Num;
    j["StringInfoNum"] = header.StringInfoNum;

    j["pData0"] = header.pData1;
    j["pData1"] = header.pData1;
    j["pStringInfo"] = header.pStringInfo;

    j["pFilename"] = header.pFilename;

    return j;
}

inline auto parseHeaderHEX(const Header& header) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    j["Magic"] = asHex(header.Magic);
    j["Version"] = asHex(header.Version);

    j["Data0Num"] = asHex(header.Data0Num);
    j["Data1Num"] = asHex(header.Data1Num);
    j["StringInfoNum"] = asHex(header.StringInfoNum);

    j["pData0"] = asHex(header.pData1);
    j["pData1"] = asHex(header.pData1);
    j["pStringInfo"] = asHex(header.pStringInfo);

    j["pFilename"] = asHex(header.pFilename);

    return j;
}

inline auto parseData0DEC(const std::vector<Data0>& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    for (auto i = 0u; i < obj.size(); i++) {
        nlohmann::ordered_json jItem;

        jItem["Unk[0]"] = obj[i].Unk[0];
        jItem["Unk[1]"] = obj[i].Unk[1];

        j[std::to_string(i)] = jItem;
    }

    return j;
}

inline auto parseData0HEX(const std::vector<Data0>& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    for (auto i = 0u; i < obj.size(); i++) {
        nlohmann::ordered_json jItem;
        std::stringstream ss;

        jItem["Unk[0]"] = asHex(obj[i].Unk[0]);
        jItem["Unk[1]"] = asHex(obj[i].Unk[1]);

        ss << "0x" << std::hex << i;

        j[ss.str()] = jItem;
    }

    return j;
}

inline auto parseData1DEC(const std::vector<Data1>& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    for (auto i = 0u; i < obj.size(); i++) {
        nlohmann::ordered_json jItem;

        jItem["Unk0"] = obj[i].Unk0;
        jItem["Index"] = obj[i].Index;
        jItem["Unk1"] = obj[i].Unk1;
        jItem["Unk2"] = obj[i].Unk2;

        j[std::to_string(i)] = jItem;
    }

    return j;
}

inline auto parseData1HEX(const std::vector<Data1>& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    for (auto i = 0u; i < obj.size(); i++) {
        nlohmann::ordered_json jItem;
        std::stringstream ss;

        jItem["Unk0"] = asHex(obj[i].Unk0);
        jItem["Index"] = asHex(obj[i].Index);
        jItem["Unk1"] = asHex(obj[i].Unk1);
        jItem["Unk2"] = asHex(obj[i].Unk2);

        ss << "0x" << std::hex << i;

        j[ss.str()] = jItem;
    }

    return j;
}

inline auto parseStringInfoDEC(const std::vector<StringInfo>& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    for (auto i = 0u; i < obj.size(); i++) {
        nlohmann::ordered_json jItem;

        jItem["pU16Str"] = obj[i].pU16Str;
        jItem["Size"] = obj[i].Size;
        jItem["SizeDuplicate"] = obj[i].SizeDuplicate;

        j[std::to_string(i)] = jItem;
    }

    return j;
}

inline auto parseStringInfoHEX(const std::vector<StringInfo>& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    for (auto i = 0u; i < obj.size(); i++) {
        nlohmann::ordered_json jItem;
        std::stringstream ss;

        jItem["pU16Str"] = asHex(obj[i].pU16Str);
        jItem["Size"] = asHex(obj[i].Size);
        jItem["SizeDuplicate"] = asHex(obj[i].SizeDuplicate);

        ss << "0x" << std::hex << i;

        j[ss.str()] = jItem;
    }

    return j;
}

inline auto parseStringsDEC(const std::vector<std::u16string>& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;
    std::wstring_convert<std::codecvt_utf8_utf16<utf16>, utf16> converter;

    for (auto i = 0u; i < obj.size(); i++) {
        std::string str;

        str = converter.to_bytes(obj[i]);

        j[std::to_string(i)] = std::move(str);
    }

    return j;
}

inline auto parseStringsHEX(const std::vector<std::u16string>& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;
    std::wstring_convert<std::codecvt_utf8_utf16<utf16>, utf16> converter;

    for (auto i = 0u; i < obj.size(); i++) {
        std::stringstream ss;
        std::string str;

        str = converter.to_bytes(obj[i]);

        ss << "0x" << std::hex << i;

        j[ss.str()] = std::move(str);
    }

    return j;
}

auto json(const cLMD& obj) -> nlohmann::ordered_json
{
    if (!obj.isValid())
        return {};

    nlohmann::ordered_json j;

    const auto& header = obj.getHeader();
    const auto& filename = obj.getFilename();
    const auto& data0 = obj.getData0();
    const auto& data1 = obj.getData1();
    const auto& stringInfo = obj.getStringInfo();
    const auto& strings = obj.getStrings();

    j["Header"] = parseHeaderHEX(header);
    j["Data0"] = parseData0HEX(data0);
    j["Data1"] = parseData1HEX(data1);
    j["StringInfo"] = parseStringInfoHEX(stringInfo);
    j["Strings"] = parseStringsHEX(strings);
    j["Filename"] = filename;

    return j;
}

auto dump(const cLMD &obj, const int indent) -> std::string
{
    return json(obj).dump(indent);
}

}; /// namespace lmd
}; /// namespace mh4u
}; /// namespace mh
