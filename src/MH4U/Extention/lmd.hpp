#pragma once
#include <string>
#include <nlohmann/json_fwd.hpp>

namespace mh {
namespace mh4u {
namespace lmd {

class cLMD;

extern auto json(const cLMD& obj) -> nlohmann::ordered_json;
extern auto dump(const cLMD& obj, const int indent = -1) -> std::string;

}; /// namespace lmd
}; /// namespace mh4u
}; /// namespace mh
